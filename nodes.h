#ifndef NODES_H
#define NODES_H

// Define a node as a linked list
template<class Type>
struct Node 
{
    Type data;
    Node<Type>* next;
    
    Node(const Type& val) 
    : data(val), next(nullptr) 
    {
    }

};

// Define a node as a doubly linked list
template<class Type>
struct NodeD
{
    Type data;
    NodeD<Type>* left;
    NodeD<Type>* right;

    NodeD(const Type& value) 
    : data(value), left(nullptr), right(nullptr)
    {
    }
};

#endif
