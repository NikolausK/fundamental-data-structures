#include "nodes.h"
#include <iostream>
using namespace std; 

#ifndef BINARYTREEDS_H
#define BINARYTREEDS_H

// Define the Binary Tree class
template<class Type>
class BinaryTree 
{
    private:

    NodeD<Type>* root;

    NodeD<Type>* insertNode
    (
        NodeD<Type>* root, 
        Type data
    ) 
    {
        if (root == nullptr)
        {
            root = new NodeD<Type>(data);
        }
        else if (data < root->data)
        {
            root->left = insertNode(root->left, data);
        }
        else
        {
            root->right = insertNode(root->right, data);
        }
        return root;
    }

    NodeD<Type>* findMin(NodeD<Type>* node)
    {
        while (node->left != nullptr) 
        {
            node = node->left;
        }
        return node;
    }

    NodeD<Type>* deleteNode
    (
        NodeD<Type>* root, 
        Type key
    )
    {
        if (root == nullptr) return root;

        if (key < root->data) 
        {
            root->left = deleteNode(root->left, key);
        } 
        else if (key > root->data) 
        {
            root->right = deleteNode(root->right, key);
        } 
        else 
        {
            if (root->left == nullptr) 
            {
                NodeD<Type>* temp = root->right;
                delete root;
                return temp;
            } 
            else if (root->right == nullptr) 
            {
                NodeD<Type>* temp = root->left;
                delete root;
                return temp;
            }
            NodeD<Type>* temp = findMin(root->right);
            root->data = temp->data;
            root->right = deleteNode(root->right, temp->data);
        }
        return root;
    }

    NodeD<Type>* searchNode
    (
        NodeD<Type>* root, 
        Type key
    ) 
    {
        if (root == nullptr || root->data == key) 
            return root;

        if (root->data < key) 
            return searchNode(root->right, key);

        return searchNode(root->left, key);
    }

    void inorderTraversal(NodeD<Type>* root, vector<int>& values)
    {
        if (root != nullptr) 
        {
            inorderTraversal(root->left, values);
            //cout << root->data << " ";
            values.push_back(root->data);
            inorderTraversal(root->right, values);
        }
    }

    public:

    // default constructor
    BinaryTree() 
    : 
    root(nullptr) 
    {
    }

    //construct from initializer list
    BinaryTree(initializer_list<Type> vals)
    : 
    root(nullptr)
    {
        for (Type vali: vals)
        {
            insert(vali);
        }
    }

    void insert(Type data) 
    {
        root = insertNode(root, data);
    }

    void remove(Type key) 
    {
        root = deleteNode(root, key);
    }

    bool search(Type key) 
    {
        return searchNode(root, key) != nullptr;
    }

    void inorder(vector<int>& values) 
    {
        inorderTraversal(root, values);
        //cout << endl;
    }

    // Check if empty
    bool isEmpty() 
    {
        return root == nullptr;
    }

    NodeD<Type>* getRoot()
    {
        return root;
    }

    // Breadth-First Search (BFS) using queue
    void BFSqueue(vector<int>& values)
    {
        if (!root) 
            return;
        
        queue<NodeD<Type>*> q;
        q.push(root);
        
        while (!q.empty())
        {
            NodeD<Type>* current = q.front();
            q.pop();
            //cout << current->data << " ";
            values.push_back(current->data);
            
            if (current->left) q.push(current->left);
            if (current->right) q.push(current->right);
        }
        //cout << endl;
    }

    // Depth-First Search (DFS) using recursion
    void DFSrecursive(NodeD<Type>* root, vector<int>& values)
    {
        if (!root)
            return;
        
        // Process the current node
        //cout << root->data << " ";
        values.push_back(root->data);
        DFSrecursive(root->left, values);
        DFSrecursive(root->right, values);
    }

    // DFS using stack
    void DFSstack(vector<int>& values)
    {
        if (!root)
            return;

        stack<NodeD<Type>*> s;
        s.push(root);

        while (!s.empty())
        {
            NodeD<Type>* current = s.top();
            s.pop();

            // Process the current node
            //cout << current->data << " ";
            values.push_back(current->data);

            if (current->right) s.push(current->right);
            if (current->left) s.push(current->left);
        }
    }

};

#endif
