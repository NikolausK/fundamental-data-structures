#include "unitTests.h"

int main()
{
    testStack();
    cout << "Passed 2 tests for stack!" << endl;

    testQueue();
    cout << "Passed 2 tests for queue!" << endl;

    testBinaryTree();
    cout << "Passed 6 tests for binary tree!" << endl;
    
    testGraph();
    cout << "Passed 4 tests for Graph!" << endl;

    return 0;
}