#include "nodes.h"
#include <iostream>
using namespace std; 

#ifndef GRAPHDS_H
#define GRAPHDS_H

template<class Type>
class Graph
{
    //each edge has the node ids on each side
    // i.e. edge std::make_tuple(0, 1) connects node 0 with node 1
    vector<tuple<Type, Type>> graphEdges;

    // each vector entry (graph node) has a vector with the connected nodes
    vector<vector<Type>> graphNodes;

    public:

    //construct from initializer list
    Graph(initializer_list<tuple<Type, Type>> edges)
    {
        for (auto edgei: edges)
        {
            graphEdges.push_back(edgei);
        }
        // convert to adjacencty list
        findNodesConnectivity();
    }

    //access function
    const vector<vector<Type>>& getGraphNodes()
    {
        return graphNodes;
    }

    //create adjacency list
    //for each node id create a list of the connected nodes
    void findNodesConnectivity()
    {
        // max element of the first element of all the tuples
        auto max_it1 = 
        max_element
        (
            graphEdges.begin(), 
            graphEdges.end()
        );
                    
        // max element based on the second element of the tuples
        auto max_it2 = 
        max_element
        (
            graphEdges.begin(), 
            graphEdges.end(),
            [](const tuple<Type, Type>& a, const tuple<Type, Type>& b)
            {
                return get<1>(a) < get<1>(b);
            }
        );

        auto maxValue = get<0>(*max_it1) > get<1>(*max_it2)?
                        get<0>(*max_it1):
                        get<1>(*max_it2);
                        
        graphNodes.resize(maxValue+1);
        //cout << "max value size " << nodes->size() << endl;
        
        //- create adjacency list
        for (size_t i=0; i<graphNodes.size(); i++)
        {
            for (auto edge: graphEdges)
            {
                if (i == get<0>(edge))
                {
                    graphNodes[i].push_back(get<1>(edge));
                }
                else if (i == get<1>(edge))
                {
                    graphNodes[i].push_back(get<0>(edge));
                }
            }
        }
    }

    // shared_ptr<vector<vector<Type>>>  findNodesConnectivity()
    // {
    //     // max element of the first element of all the tuples
    //     auto max_it1 = 
    //     max_element
    //     (
    //         graphEdges.begin(), 
    //         graphEdges.end()
    //     );
                    
    //     // max element based on the second element of the tuples
    //     auto max_it2 = 
    //     max_element
    //     (
    //         graphEdges.begin(), 
    //         graphEdges.end(),
    //         [](const tuple<Type, Type>& a, const tuple<Type, Type>& b)
    //         {
    //             return get<1>(a) < get<1>(b);
    //         }
    //     );

    //     auto maxValue = get<0>(*max_it1) > get<1>(*max_it2)?
    //                     get<0>(*max_it1):
    //                     get<1>(*max_it2);
                        
    //     auto nodes = make_shared<vector<vector<Type>>>(maxValue+1);
    //     //cout << "max value size " << nodes->size() << endl;
        
    //     //- create adjacency list
    //     for (size_t i=0; i<nodes->size(); i++)
    //     {
    //         for (auto edge: graphEdges)
    //         {
    //             if (i == get<0>(edge))
    //             {
    //                 (*nodes)[i].push_back(get<1>(edge));
    //             }
    //             else if (i == get<1>(edge))
    //             {
    //                 (*nodes)[i].push_back(get<0>(edge));
    //             }
    //         }
    //     }

    //     return nodes;
    // }

    //- breadth first search, use queue     
    void BFSqueue
    (
        vector<Type>& nodesPath, 
        Type startId,
        Type endId
    )
    {
        queue<Type> q;
        vector<bool> visited(graphNodes.size(), false);
        
        //- start with the id of the start node
        q.push(startId);
        visited[startId] = true;
        //nodesPath.push_back(startId);
        
        while (!q.empty())
        {
            int node = q.front();
            nodesPath.push_back(node);
            q.pop();
            //cout << node << " ";
            
            // Explore all neighbors of node 
            for (int neighbor : graphNodes[node])
            {
                if (neighbor == endId)
                {
                    nodesPath.push_back(neighbor);
                    //cout << neighbor << "     ";
                    return;
                }
                
                if (!visited[neighbor])
                {
                    q.push(neighbor);
                    visited[neighbor] = true;
                }
            }
        }
        //std::cout << std::endl;
    }
    
    //- depth first search, use stack 
    void DFSstack
    (
        vector<Type>& nodesPath, 
        Type startId,
        Type endId
    )
    {
        stack<int> s;
        vector<bool> visited(graphNodes.size(), false);
        
        //- start with the id of the start node
        s.push(startId);
        //visited[startId] = true;
        
        while (!s.empty())
        {
            Type node = s.top();
            //nodesPath.push_back(node);
            s.pop();
            
            if (!visited[node])
            {
                visited[node] = true;
                // cout << node << " ";
                nodesPath.push_back(node);
                
                for 
                (
                    auto it = graphNodes[node].rbegin(); 
                    it != graphNodes[node].rend(); 
                    ++it
                ) 
                {
                    if (*it == endId)
                    {
                        nodesPath.push_back(*it);
                        //cout << *it << endl;
                        return;
                    }
                    if (!visited[*it]) 
                    {
                        s.push(*it);
                    }
                }    
            }
        }
    }
    
    // Depth-First Search (DFS) for a Graph using recursion
    void dfsGraph
    (
        vector<Type>& nodesPath, 
        Type startId, 
        vector<bool>& visited
    )
    {
        visited[startId] = true;
        nodesPath.push_back(startId);
        // std::cout << node << " ";

        for (Type neighbor : graphNodes[startId])
        {
            if (!visited[neighbor]) 
            {
                dfsGraph(nodesPath, neighbor, visited);
            }
        }
    }

    void DFSrecursive(vector<Type>& nodesPath, Type startId)
    {
        std::vector<bool> visited(graphNodes.size(), false);
        dfsGraph(nodesPath, startId, visited);
        // std::cout << std::endl;
    }

};

#endif
