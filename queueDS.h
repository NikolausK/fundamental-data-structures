#include "nodes.h"
#include <iostream>
using namespace std; 

#ifndef QUEUEDS_H
#define QUEUEDS_H

// Define the queue class
template<class Type>
class Queue 
{
    private:

    Node<Type>* front; // Pointer to the front of the queue
    Node<Type>* rear; // Pointer to the rear of the queue

    public:

    // default Constructor
    Queue() 
    : 
    front(nullptr),
    rear(nullptr) 
    {
    }

    // Construct from initializer list
    Queue(initializer_list<Type> vals) 
    : 
    front(nullptr), 
    rear(nullptr) 
    {
        for (Type vali: vals)
        {
            enqueue(vali);
        }
    }

    // Destructor
    ~Queue() 
    {
        // Deallocate memory for all nodes
        while (front != nullptr) 
        {
            Node<Type>* temp = front;
            front = front->next;
            delete temp;
        }
    }

    // Enqueue: Add to the rear of the queue
    void enqueue(Type val) 
    {
        Node<Type>* newNode = new Node<Type>(val);
        if (isEmpty()) 
        {
            front = rear = newNode;
        } 
        else 
        {
            rear->next = newNode;
            rear = newNode;
        }
    }

    // Dequeue: Remove from the front and return value
    Type dequeue() 
    {
        if (isEmpty()) 
        {
            cerr << "Error: Queue is empty!" << endl;
            return Type();
        } 
        else 
        {
            Type val = front->data;
            Node<Type>* temp = front;
            front = front->next;
            delete temp;
            if (front == nullptr) 
            {
                rear = nullptr;
            }
            return val;
        }
    }

    // Peek: Return the element on the front
    Type peek() 
    {
        if (isEmpty()) 
        {
            cerr << "Error: Queue is empty!" << endl;
            return Type();
        } 
        else 
        {
            return front->data;
        }
    }

    // Check if empty
    bool isEmpty() 
    {
        return front == nullptr;
    }
};

#endif
