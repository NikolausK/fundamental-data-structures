#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <regex>
#include <list>

#include "stackDS.h"
#include "queueDS.h"
#include "binaryTreeDS.h"
#include "graphDS.h"

using namespace std;

void testStack()
{
    // Create a stack instance
    Stack<float> stack;

    // Push elements onto the stack
    stack.push(1.58);
    stack.push(2.4);
    stack.push(7.33);

    double tolerance = 1e-7;
    assert(fabs(stack.peek() - 7.33)<tolerance 
        && "Values during peek don't match");
    assert(fabs(stack.pop() - 7.33)<tolerance 
        && "Values during pop don't match");
    assert(fabs(stack.peek() - 2.4)<tolerance 
        && "Values during peek don't match");
    assert(fabs(stack.pop() - 2.4)<tolerance 
        && "Values during pop don't match");
    assert(fabs(stack.peek() - 1.58)<tolerance 
        && "Values during peek don't match");
    assert(fabs(stack.pop() - 1.58)<tolerance 
        && "Values during pop don't match");
    assert(stack.isEmpty() == true 
        && "Stack is not empty as it should be");
    //cout << "Peek: " << stack.peek() << endl; // Should print an error message

    Stack<int> stack2(initializer_list<int>{1, 45, 53, 90, 849});
    list<int> expectedList = {1, 45, 53, 90, 849};
    while (!stack2.isEmpty())
    {
        int last = expectedList.back();
        expectedList.pop_back();
        assert( fabs(stack2.pop() - last)<tolerance 
            && "Values during pop2 don't match");
    }
}

void testQueue()
{
    // Create a queue instance
    Queue<int> queue;

    // Enqueue elements onto the queue
    queue.enqueue(10);
    queue.enqueue(20);
    queue.enqueue(30);

    // Peek and dequeue elements from the queue
    assert(queue.peek() == 10 
        && "Values during queue peek don't match");
    assert(queue.dequeue() == 10 
        && "Values during dequeue don't match");
    assert(queue.peek() == 20 
        && "Values during queue peek don't match");
    assert(queue.dequeue() == 20 
        && "Values during dequeue don't match");
    assert(queue.peek() == 30 
        && "Values during queue peek don't match");
    assert(queue.dequeue() == 30 
        && "Values during dequeue don't match");
    assert(queue.isEmpty() == true 
        && "Queue is not empty as it should be");
    //cout << "Peek: " << queue.peek() << endl; // Should print an error message

    Queue<int> queue2(initializer_list<int>{13, 4, 3, 94});
    list<int> expectedList = {13, 4, 3, 94};
    while (!queue2.isEmpty())
    {
        int first = expectedList.front();
        expectedList.pop_front();
        assert(queue2.dequeue() == first
            && "Values during dequeue2 don't match");
    }
}

void testBinaryTree()
{
    BinaryTree<int> tree;
    tree.insert(10);
    tree.insert(5);
    tree.insert(15);
    tree.insert(7);
    tree.insert(3);

    vector<int> val;
    tree.inorder(val);

    vector<int> expectedVctr = {3, 5, 7, 10, 15};
    assert(val == expectedVctr
        && "Values after inorder traversal don't match");

    int key = 7;
    assert(tree.search(key) == true
        && "Couldn't find value 7 in binary tree!");

    tree.remove(key);
    val.clear();
    auto it = remove(expectedVctr.begin(), expectedVctr.end(), 7);
    expectedVctr.erase(it, expectedVctr.end());
    tree.inorder(val);
    assert(val == expectedVctr
        && "Values after 2nd inorder traversal don't match");

    // starting tests for BFS and DFS search
    initializer_list<int> inList{13, 4, 3, 94, 128, 432, 70};
    BinaryTree<int> tree2(inList);


    val.clear();
    tree2.BFSqueue(val);
    expectedVctr = {13, 4, 94, 3, 70, 128, 432};
    assert(val == expectedVctr
        && "Values after BFSqueue don't match");

    val.clear();
    tree2.DFSstack(val);
    expectedVctr = {13, 4, 3, 94, 70, 128, 432};
    assert(val == expectedVctr
        && "Values after DFSstack don't match");
    
    val.clear();
    tree2.DFSrecursive(tree2.getRoot(), val);
    assert(val == expectedVctr
        && "Values after DFSrecursive don't match");


    vector<vector<int>> expectedInorder
    {
        {3, 4, 70, 94, 128, 432}, 
        {3, 70, 94, 128, 432}, 
        {70, 94, 128, 432}, 
        {70, 128, 432}, 
        {70, 432}, 
        {70}, 
        {} 
    };

    int idx = 0;
    for (int i: inList)
    {
        if (tree2.search(i))
        {
            tree2.remove(i);
            // cout << "Inorder Traversal after deleting " 
            //           << i << ": ";
            val.clear();
            tree2.inorder(val);
            assert(val == expectedInorder[idx]
                && "Values after last inorder don't match");
            idx++;
        }
    }
}

void testGraph()
{
    Graph graph1 = Graph
    (
        initializer_list<tuple<int, int>>
        { 
            std::make_tuple(0, 1),
            std::make_tuple(1, 5),
            std::make_tuple(1, 2), 
            std::make_tuple(2, 3),
            std::make_tuple(3, 4),
            std::make_tuple(3, 5),
            std::make_tuple(5, 4)
        }
    );

    vector<vector<int>> nodes = graph1.getGraphNodes();
    vector<vector<int>> expectedNodes
    { 
        {1}, 
        {0, 5, 2}, 
        {1, 3}, 
        {2, 4, 5}, 
        {3, 5}, 
        {1, 3, 4}
    };

    int idx = 0;
    for (size_t i=0; i<nodes.size(); i++)
    {
        assert(nodes[i] == expectedNodes[i]
            && "Adjacency list is not the expected");
        idx++;
    }

    vector<int> path;
    graph1.BFSqueue(path, 0, 5);
    vector<int> expectedPath{0, 1, 5};
    assert(path == expectedPath
        && "vector after BFSqueue is not the expected");

    path.clear();
    graph1.DFSstack(path, 0, 5);
    assert(path == expectedPath
        && "vector after DFSstack is not the expected");

    path.clear();
    expectedPath = {0, 1, 5, 3, 2, 4};
    graph1.DFSrecursive(path, 0);
    assert(path == expectedPath
        && "vector after DFSrecursive is not the expected");
}

//auxiliary function converting a string to a vector
shared_ptr<std::vector<int>> string2vector(const string& input)
{
    string cleanedString = regex_replace(input, regex(" "), "");

    shared_ptr<vector<int>> values;
    regex numberRegex("\\d+"); // Matches individual numbers
    sregex_iterator begin(input.begin(), input.end(), numberRegex);
    sregex_iterator end;

    for_each
    (
        begin, 
        end, 
        [&values](const smatch& match) 
        {
            values->push_back(stoi(match.str()));
        }
    );

    return values;
}
