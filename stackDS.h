#include "nodes.h"
#include <iostream>
using namespace std; 

#ifndef STACKDS_H
#define STACKDS_H

// Define the stack class
template<class Type>
class Stack 
{
    private:
    
    Node<Type>* top; // Pointer to the top of the stack
        
    public:

    // default Constructor
    Stack() 
    : top(nullptr) 
    {
    }

    //construct from initializer list
    Stack(initializer_list<Type> vals)
    : top(nullptr)
    {
        for (Type vali: vals)
        {
            push(vali);
        }
    }

    // Destructor
    ~Stack() 
    {
        while (top != nullptr) 
        {
            Node<Type>* temp = top;
            top = top->next;
            delete temp;
        }
    }

    // Push: Add to the top
    void push(const Type& val) 
    {
        Node<Type>* newNode = new Node<Type>(val);
        newNode->next = top;
        top = newNode;
    }

    // Pop: Remove from the top and return value
    Type pop() 
    {
        if (isEmpty()) 
        {
            cerr << "Error: Stack is empty!" << endl;
            return Type();
        } 
        else 
        {
            Type val = top->data;
            Node<Type>* temp = top;
            top = top->next;
            delete temp;
            return val;
        }
    }

    // Peek: Return the element on the top
    Type peek() 
    {
        if (isEmpty()) 
        {
            cerr << "Error: Stack is empty!" << endl;
            return Type();
        } 
        else 
        {
            return top->data;
        }
    }

    // Check if empty
    bool isEmpty() 
    {
        return top == nullptr;
    }
};

#endif
